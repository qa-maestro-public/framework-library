nvme.identify = function (cns, cntid, nsid) {
  const buffer = Buffer.alloc(4096)
  const cqe = nvme.sendAdminCommand(
    {
      opc: 0x06,
      nsid: nsid,
      cdw10: (cntid << 16) + cns
    },
    buffer,
    false
  )
  return { cqe, buffer }
}

nvme.identifyController = function () {
  const cns = 1
  const cntid = 0
  const nsid = 0
  const { cqe, buffer } = nvme.identify(cns, cntid, nsid)
  const data = nvmeExt.parseIdentifyControllerData(buffer)
  return { cqe, buffer, data }
}

nvme.identifyNamespace = function (nsid) {
  const cns = 0
  const cntid = 0
  const { cqe, buffer } = nvme.identify(cns, cntid, nsid)
  const data = nvmeExt.parseIdentifyNamespaceData(buffer)
  return { cqe, buffer, data }
}

nvme.identifyActiveControllers = function (nsid, cntid = 0) {
  const cns = 0x12
  const { cqe, buffer } = nvme.identify(cns, cntid, nsid)
  const controllers = nvmeExt.parseControllerList(buffer)
  return { cqe, buffer, controllers }
}

nvme.identifyAllocatedNamespaces = function (nsid = 0) {
  const cns = 0x10
  const cntid = 0
  const { cqe, buffer } = nvme.identify(cns, cntid, nsid)
  const namespaces = nvmeExt.parseNamespaceList(buffer)
  return { cqe, buffer, namespaces }
}

nvme.identifyActiveNamespaces = function (nsid = 0) {
  const cns = 0x02
  const cntid = 0
  const { cqe, buffer } = nvme.identify(cns, cntid, nsid)
  const namespaces = nvmeExt.parseNamespaceList(buffer)
  return { cqe, buffer, namespaces }
}

nvme.identifyAllocatedNamespace = function (nsid) {
  const cns = 0x11
  const cntid = 0
  const { cqe, buffer } = nvme.identify(cns, cntid, nsid)
  const data = nvmeExt.parseIdentifyNamespaceData(buffer)
  return { cqe, buffer, data }
}
