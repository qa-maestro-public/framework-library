/**
 * Sends an NVMe Set Features command.
 *
 * @param {Number} fid - Feature Identifier.
 * @param {Buffer|null} data - Data buffer pointer. If no data structure is used as part of the specified feature, this is null.
 * @param {Object} [options] - Additional options.
 * @param {Boolean} [options.save=false] - Indicates if the controller shall save the attribute.
 * @param {Number} [options.uuidIndex] - UUID Index value. Only required if the controller supports UUID selection by the Set Features command.
 * @param {Object} [options.cdw] - Feature specific Command Dwords.
 * @param {Number} [options.cdw.cdw11] - Feature specific Command Dword 11.
 * @param {Number} [options.cdw.cdw12] - Feature specific Command Dword 12.
 * @param {Number} [options.cdw.cdw13] - Feature specific Command Dword 13.
 * @param {Number} [options.cdw.cdw15] - Feature specific Command Dword 15.
 *
 * @returns {nvme.CompletionQueueEntry} - Returns the result of the NVMe command.
 */
nvme.setFeature = function (fid, data = null, options = {}) {
  const { save = false, uuidIndex, cdw = {} } = options

  // Define the opcode for Set Features (assuming you know the opcode, let's say it's 0x09 for the sake of this example)
  const OPC_SET_FEATURE = 0x09

  // Construct the command based on the NVMe spec details provided
  const command = {
    nsid: 0,
    opc: OPC_SET_FEATURE,
    cdw10: (save ? 1 << 31 : 0) | fid
  }

  if (uuidIndex !== undefined) {
    command.cdw14 = uuidIndex & 0x7f // Only 7 bits for UUID Index as per spec
  }

  // Add feature-specific cdws if provided
  if (cdw.cdw11 !== undefined) command.cdw11 = cdw.cdw11
  if (cdw.cdw12 !== undefined) command.cdw12 = cdw.cdw12
  if (cdw.cdw13 !== undefined) command.cdw13 = cdw.cdw13
  if (cdw.cdw15 !== undefined) command.cdw15 = cdw.cdw15

  // Send command with or without data pointer
  return nvme.sendAdminCommand(command, data, true)
}

/**
 * Sends an NVMe Set Features command specifically for the Arbitration feature.
 *
 * @param {Number} hpw - High Priority Weight. Defines the number of commands that may be executed
 *                       from the high priority service class in each arbitration round. This is a 0’s based value.
 * @param {Number} mpw - Medium Priority Weight. Defines the number of commands that may be executed
 *                       from the medium priority service class in each arbitration round. This is a 0’s based value.
 * @param {Number} lpw - Low Priority Weight. Defines the number of commands that may be executed
 *                       from the low priority service class in each arbitration round. This is a 0’s based value.
 * @param {Number} ab  - Arbitration Burst. Indicates the maximum number of commands that the controller may
 *                       fetch at one time from a particular Submission Queue. Expressed as a power of two.
 * @param {Object} [options] - Additional options for the `setFeature` function.
 * @param {Boolean} [options.save=false] - Indicates if the controller shall save the attribute so it persists
 *                                         through all power states and resets.
 * @param {Number} [options.uuidIndex=0] - UUID Index for the command if applicable.
 *
 * @returns {nvme.CompletionQueueEntry} - Returns the result of the NVMe command.
 */
nvme.setFeatureArbitration = function (hpw, mpw, lpw, ab, options = {}) {
  const fid = 0x01 // Feature Identifier for Arbitration

  const cdw11 =
    ((hpw & 0xff) << 24) |
    ((mpw & 0xff) << 16) |
    ((lpw & 0xff) << 8) |
    (ab & 0x07)

  return nvme.setFeature(fid, {
    save: options.save || false,
    uuidIndex: options.uuidIndex || 0,
    cdw11: cdw11
  })
}

/**
 * @typedef PowerManagementFeatureResponse
 * @type {object}
 * @property {CompletionQueueEntry} cqe - Completion Queue Entry resulting from the Set Features command for Power Management.
 * @property {Object} parsedData - Parsed details from the cqe, specific to the Power Management feature.
 * @property {Number} parsedData.wh - Workload Hint.
 * @property {Number} parsedData.ps - Power State.
 * @memberof nvme
 */

/**
 * Sends an NVMe Set Features command specifically for the Power Management feature.
 *
 * @param {Number} wh - Workload Hint. Indicates the type of workload expected.
 * @param {Number} ps - Power State. Indicates the new power state to transition.
 * @param {Object} [options] - Additional options for the `setFeature` function.
 * @param {Boolean} [options.save=false] - If the controller shall save the attribute.
 * @param {Number} [options.uuidIndex=0] - UUID Index for the command.
 *
 * @returns {nvme.PowerManagementFeatureResponse} - Returns the result of the NVMe command along with parsed WH and PS.
 * @memberof nvme
 */
nvme.setFeaturePowerManagement = function (wh, ps, options = {}) {
  const fid = 0x02 // Feature Identifier for Power Management

  const cdw11 = ((wh & 0x07) << 5) | (ps & 0x1f)

  return nvme.setFeature(fid, {
    save: options.save || false,
    uuidIndex: options.uuidIndex || 0,
    cdw11: cdw11
  })
}
