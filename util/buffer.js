utils.buffer = {}
utils.buffer.toAsciiString = function (buffer) {
  let string = ''
  for (const value of buffer.values()) {
    string += String.fromCharCode(value <= 0x20 ? 0 : value > 0x7f ? 0 : value)
  }
  return string
}
utils.buffer.writeBigIntLE = function (buffer, offset, bigint, size) {
  while (size > 0) {
    const value = Number(bigint & 0xffn)
    bigint = bigint >> 8n
    buffer.writeUInt8(value, offset)
    size -= 1
    offset += 1
  }
}
utils.buffer.writeBigIntBE = function (buffer, offset, bigint, size) {
  while (size > 0) {
    size -= 1
    const value = Number(bigint & 0xffn)
    bigint = bigint >> 8n
    buffer.writeUInt8(value, offset + size)
  }
}
