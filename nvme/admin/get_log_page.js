importScript('./get_log_page_parsers.js')

/**
 * Retrieves a specific log page from an NVMe device.
 *
 * @function getLogPage
 * @memberof nvme
 * @param {number} lid - The log page identifier.
 * @param {number} numd - Represents the number of 4-byte DWORDS the host requests.
 * @param {number} nsid - The namespace identifier. Set to 0xFFFFFFFF for all namespaces.
 * @param {Object} [options] - Additional options based on NVMe Base Spec 2.0.
 * @param {number} [options.rae=0] - Retain Asynchronous Event.
 * @param {number} [options.lsp=0] - Log Specific Field.
 * @param {number} [options.lsi=0] - Log Specific Identifier.
 * @param {number} [options.lpol=0] - Log Page Offset Lower.
 * @param {number} [options.lpou=0] - Log Page Offset Upper.
 * @param {number} [options.csi=0] - Command Set Identifier.
 * @param {number} [options.ot=0] - Obsolete Table.
 * @param {number} [options.uuid=0] - UUID Index.
 * @returns {Object} Contains completion queue entry (cqe) and the log page buffer.
 * @throws Will throw an error if the admin command fails.
 */
nvme.getLogPage = function (lid, numd, nsid, options) {
  const cmd = {}
  // NOTE: the following options are introduced from NVMe Base Spec 2.0
  const {
    rae = 0,
    lsp = 0,
    lsi = 0,
    lpol = 0,
    lpou = 0,
    csi = 0,
    ot = 0,
    uuid = 0
  } = options || {}

  numd -= 1 // combined NUMDL and NUMDU makes 0's based value
  const numdl = (numd >>> 0) & 0xffff
  const numdu = (numd >>> 16) & 0xffff
  cmd.opc = nvmeExt.OPCODE.ADMIN.GET_LOG_PAGE
  cmd.nsid = nsid
  cmd.cdw10 =
    (lid & 0xff) |
    ((lsp & 0xf) << 8) |
    ((rae & 0x1) << 15) |
    ((numdl & 0xffff) << 16)
  cmd.cdw11 = (numdu & 0xffff) | ((lsi & 0xffff) << 16)
  cmd.cdw12 = lpol
  cmd.cdw13 = lpou
  cmd.cdw14 = ((csi & 0xff) << 24) | ((ot & 0x1) << 23) | (uuid & 0x7f)
  cmd.cdw15 = 0 // Reserved

  const buffer = Buffer.alloc(numdl * 4)
  const cqe = nvme.sendAdminCommand(cmd, buffer, false)
  return { cqe, buffer }
}

/**
 * @typedef SmartData
 * @property {number} criticalWarning
 * @property {number} compositeTemperature
 * @property {number} availableSpare
 * @property {number} availableSpareThreshold
 * @property {number} percentageUsed
 * @property {number} enduranceGroupCritical
 * @property {BigInt} dataUnitRead
 * @property {BigInt} dataUnitWritten
 * @property {BigInt} numHostReadCmd
 * @property {BigInt} numHostWriteCmd
 * @property {BigInt} controllerBusyTime
 * @property {BigInt} numPowerCycles
 * @property {BigInt} hoursPowerOn
 * @property {BigInt} numUnsafeShutdown
 * @property {BigInt} numUnrecoveredIntegrity
 * @property {BigInt} numErrLifecycle
 * @property {number} warningCompositeTempTime
 * @property {number} criticalCompositeTempTime
 * @memberof nvme
 */

/**
 * @typedef SmartLogResult
 * @property {nvme.CompletionQueueEntry} cqe - Completion queue entry.
 * @property {Buffer} buffer - The raw log page buffer.
 * @property {nvme.SmartData} smart - The parsed SMART data.
 * @memberof nvme
 */

/**
 * Retrieves the SMART/Health log page from an NVMe device.
 *
 * @function
 * @memberof nvme
 * @param {number} [nsid=0xffffffff] - The namespace identifier. Default is set to 0xFFFFFFFF for all namespaces.
 * @returns {nvme.SmartLogResult} Contains completion queue entry (cqe), the raw log page buffer, and the parsed SMART data.
 * @throws Will throw an error if the log page retrieval or SMART parsing fails.
 */
nvme.getLogPageSmart = function (nsid = 0xffffffff) {
  const lid = nvmeExt.LOG_IDENTIFIER.SMART
  const numd = 128
  const { cqe, buffer } = nvme.getLogPage(lid, numd, nsid)
  const smart = nvmeExt.parseSmartLog(buffer)
  return { cqe, buffer, smart }
}

/**
 * @typedef {Object} FirmwareSlotInfo
 * @property {number} nextActiveFirmwareSlot - The firmware slot that is going to be activated at the next Controller Level Reset.
 * @property {number} currentlyActiveFirmwareSlot - The firmware slot from which the actively running firmware revision was loaded.
 * @property {string[]} firmwareRevisions - An array of firmware revisions for each slot.
 * @memberof nvme
 */

/**
 * @typedef {Object} FirmwareSlotInformationLogPageResult
 * @property {nvme.CompletionQueueEntry} cqe - Completion queue entry.
 * @property {Buffer} buffer - Raw log page buffer.
 * @property {FirmwareSlotInfo} parsed - Parsed Firmware Slot Information data.
 * @memberof nvme
 */

/**
 * Retrieves the Firmware Slot Information log page from an NVMe device.
 *
 * @function
 * @returns {FirmwareSlotInformationLogPageResult} - Contains completion queue entry (cqe), the raw log page buffer, and the parsed Firmware Slot Information data.
 * @throws Will throw an error if the log page retrieval or parsing fails.
 * @memberof nvme
 */
nvme.getFirmwareSlotInformationLogPage = function () {
  const logPageCode = 0x03 // Log page code for Firmware Slot Information
  const result = nvme.getLogPage(logPageCode, 512, 0xffffffff) // Assuming nsid as all namespaces (0xFFFFFFFF)
  const parsedData = nvmeExt.parseFirmwareSlotInformation(result.buffer)

  return {
    cqe: result.cqe,
    buffer: result.buffer,
    parsed: parsedData
  }
}
