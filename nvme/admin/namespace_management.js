nvme.namespaceManagement = function (select, options) {
  const isHostToDevice = true
  let buffer
  let { nsid = 0xffffffff } = options || {}
  if (select === 0) {
    nsid = 0
    buffer = Buffer.alloc(4096)
    let { nsze, ncap, flbas = 0, dps = 0, nmic = 0 } = options || {}
    nsze = BigInt(nsze)
    ncap = BigInt(ncap)
    buffer.writeUInt32LE(Number(nsze & 0xffffffffn), 0)
    buffer.writeUInt32LE(Number(nsze >> 32n), 4)
    buffer.writeUInt32LE(Number(ncap & 0xffffffffn), 8)
    buffer.writeUInt32LE(Number(ncap >> 32n), 12)
    buffer.writeUInt8(flbas, 26)
    buffer.writeUInt8(dps, 29)
    buffer.writeUInt8(nmic, 30)
  } else {
    buffer = Buffer.alloc(0)
  }
  const cqe = nvme.sendAdminCommand(
    {
      opc: 0x0d,
      nsid: nsid,
      cdw10: select
    },
    buffer,
    isHostToDevice
  )
  return { cqe, buffer }
}

nvme.namespaceAttachment = function (select, nsid, controllers) {
  const buffer = Buffer.alloc(4096)
  buffer.writeUInt16LE(controllers.length, 0)
  controllers.forEach((id, index) => {
    buffer.writeUInt16LE(id, 2 + index * 2)
  })
  const isHostToDevice = true
  const cqe = nvme.sendAdminCommand(
    {
      opc: 0x15,
      nsid: nsid,
      cdw10: select
    },
    buffer,
    isHostToDevice
  )
  return { cqe }
}
