importScript('/lib/util/index.js')
nvmeExt.parseIdentifyControllerData = function (buffer) {
  const data = {}
  data.VID = buffer.readUInt16LE(0)
  data.SSVID = buffer.readUInt16LE(2)
  data.SN = utils.buffer.toAsciiString(buffer.slice(4, 24))
  data.MN = utils.buffer.toAsciiString(buffer.slice(24, 64))
  data.FR = utils.buffer.toAsciiString(buffer.slice(64, 72))
  data.RAB = buffer.readUInt8(72)
  data.IEEE =
    buffer.readUInt8(73) +
    (buffer.readUInt8(74) << 8) +
    (buffer.readUInt8(75) << 16)
  data.CMIC = buffer.readUInt8(76)
  data.MDTS = buffer.readUInt8(77)
  data.CNTLID = buffer.readUInt16LE(78)
  const VER = buffer.readUInt32LE(80)
  const VER_MJR = VER >>> 16
  const VER_MNR = (VER >>> 8) & 0xff
  const VER_TER = VER & 0xff
  data.VER = { MJR: VER_MJR, MNR: VER_MNR, TER: VER_TER }
  data.RTD3R = buffer.readUInt32LE(84)
  data.RTD3E = buffer.readUInt32LE(88)
  data.OAES = buffer.readUInt32LE(92)
  data.OACS = buffer.readUInt16LE(256)
  data.ACL = buffer.readUInt8(258)
  data.AERL = buffer.readUInt8(259)
  data.FRMW = buffer.readUInt8(260)
  data.LPA = buffer.readUInt8(261)
  data.ELPE = buffer.readUInt8(262)
  data.NPSS = buffer.readUInt8(263)
  data.AVSCC = buffer.readUInt8(264)
  data.APSTA = buffer.readUInt8(265)
  data.WCTEMP = buffer.readUInt16LE(266)
  data.CCTEMP = buffer.readUInt16LE(268)
  data.MTFA = buffer.readUInt16LE(270)
  data.HMPRE = buffer.readUInt32LE(272)
  data.HMMIN = buffer.readUInt32LE(276)

  data.TNVMCAP =
    BigInt(buffer.readUInt32LE(280)) +
    (BigInt(buffer.readUInt32LE(284)) << 32n) +
    (BigInt(buffer.readUInt32LE(288)) << 64n) +
    (BigInt(buffer.readUInt32LE(292)) << 96n)
  data.UNVMCAP =
    BigInt(buffer.readUInt32LE(296)) +
    (BigInt(buffer.readUInt32LE(300)) << 32n) +
    (BigInt(buffer.readUInt32LE(304)) << 64n) +
    (BigInt(buffer.readUInt32LE(308)) << 96n)

  data.RPMBS = buffer.readUInt32LE(312)
  data.SQES = buffer.readUInt8(512)
  data.CQES = buffer.readUInt8(513)
  data.NN = buffer.readUInt32LE(516)
  data.ONCS = buffer.readUInt16LE(520)
  data.FUSES = buffer.readUInt16LE(522)
  data.FNA = buffer.readUInt8(524)
  data.VWC = buffer.readUInt8(525)
  data.AWUN = buffer.readUInt16LE(526)
  data.AWUPF = buffer.readUInt16LE(528)
  data.NVSCC = buffer.readUInt8(530)
  data.ACWU = buffer.readUInt16LE(532)
  data.SGLS = buffer.readUInt32LE(536)
  // TODO: Power States
  return data
}

nvmeExt.parseIdentifyNamespaceData = function (buffer) {
  const data = {}

  data.NSZE =
    BigInt(buffer.readUInt32LE(0)) + (BigInt(buffer.readUInt32LE(4)) << 32n)

  data.NCAP =
    BigInt(buffer.readUInt32LE(8)) + (BigInt(buffer.readUInt32LE(12)) << 32n)

  data.NUSE =
    BigInt(buffer.readUInt32LE(16)) + (BigInt(buffer.readUInt32LE(20)) << 32n)

  const NSFEAT = buffer.readUInt8(24)
  data.NSFEAT = {
    THINP: NSFEAT & 0x1,
    NSABP: (NSFEAT >>> 1) & 0x1,
    DAE: (NSFEAT >>> 2) & 0x1,
    UIDREUSE: (NSFEAT >>> 3) & 0x1,
    OPTPERF: (NSFEAT >>> 4) & 0x1
  }

  data.NLBAF = buffer.readUInt8(25)

  data.FLBAS = buffer.readUInt8(26)

  data.MC = buffer.readUInt8(27)

  const DPC = buffer.readUInt8(28)
  data.DPC = {
    PIT1S: DPC & 0x1,
    PIT2S: (DPC >>> 1) & 0x1,
    PIT3S: (DPC >>> 2) & 0x1,
    PIIFB: (DPC >>> 3) & 0x1,
    PIILB: (DPC >>> 4) & 0x1
  }

  const DPS = buffer.readUInt8(29)
  data.DPS = {
    PIT: DPS & 0x7,
    PIP: (DPS >>> 3) & 0x1
  }

  data.NMIC = buffer.readUInt8(30)

  data.RESCAP = buffer.readUInt8(31)

  data.FPI = buffer.readUInt8(32)

  data.DLFEAT = buffer.readUInt8(32)

  data.NVMCAP =
    BigInt(buffer.readUInt32LE(48)) +
    (BigInt(buffer.readUInt32LE(52)) << 32n) +
    (BigInt(buffer.readUInt32LE(56)) << 64n) +
    (BigInt(buffer.readUInt32LE(60)) << 96n)

  data.LBAF = []
  for (let lbaf = 0; lbaf <= data.NLBAF; lbaf++) {
    const value = buffer.readUInt32LE(128 + lbaf * 4)
    const lbafData = {
      MS: (value >>> 0) & 0xffff,
      LBADS: (value >>> 16) & 0xff,
      RP: (value >>> 24) & 0x3
    }
    data.LBAF.push(lbafData)
  }

  return data
}

nvmeExt.parseNamespaceList = function (buffer) {
  const namespaces = []
  for (let i = 0; i < 4096; i += 4) {
    const id = buffer.readUInt32LE(i)
    if (id === 0) {
      break
    }
    namespaces.push(id)
  }
  return namespaces
}

nvmeExt.parseControllerList = function (buffer) {
  const controllers = []
  for (let i = 0; i < 4096; i += 2) {
    const id = buffer.readUInt16LE(i)
    if (id === 0) {
      break
    }
    controllers.push(id)
  }
  return controllers
}
