nvmeExt.write = function (qid, startLba, numBlocks, options) {
  startLba = BigInt(startLba)
  let { buffer = null } = options || {}
  const lbaSize = 512
  const startLbaHigh = Number(startLba >> 32n)
  const startLbaLow = Number(startLba & 0xffffffffn)

  if (buffer === null) {
    buffer = Buffer.alloc(numBlocks * 512, 0)
    for (let blockIndex = 0; blockIndex < numBlocks; blockIndex += 1) {
      const dataHigh = Number((startLba + BigInt(blockIndex)) >> 32n)
      const dataLow = Number((startLba + BigInt(blockIndex)) & 0xffffffffn)
      for (let byteIndex = 0; byteIndex < lbaSize; byteIndex += 8) {
        const offset = byteIndex + blockIndex * 512
        buffer.writeUInt32BE(dataHigh, offset)
        buffer.writeUInt32BE(dataLow, offset + 4)
      }
    }
  }

  const cqe = nvme.sendNvmCommand(
    qid,
    {
      opc: 0x01,
      nsid: 1,
      cdw10: startLbaLow,
      cdw11: startLbaHigh,
      cdw12: numBlocks - 1
    },
    buffer,
    true
  )
  return { cqe, buffer }
}

nvmeExt.read = function (qid, startLba, numBlocks) {
  startLba = BigInt(startLba)
  const startLbaHigh = Number(startLba >> 32n)
  const startLbaLow = Number(startLba & 0xffffffffn)
  const buffer = Buffer.alloc(numBlocks * 512, 0)
  const cqe = nvme.sendNvmCommand(
    qid,
    {
      opc: 0x02,
      nsid: 1,
      cdw10: startLbaLow,
      cdw11: startLbaHigh,
      cdw12: numBlocks - 1
    },
    buffer,
    false
  )
  return { cqe, buffer }
}
