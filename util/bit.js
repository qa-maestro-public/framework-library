utils.bit = {}
utils.bit.mergeBigInteger = function (numArray) {
  let mergedInt = 0n

  for (let i = 0n; i < numArray.length; i++) {
    mergedInt |= BigInt(numArray[i]) << (32n * i)
  }

  return mergedInt
}
