importScript('/lib/util/index.js')

nvmeExt.parseSmartLog = function (buffer) {
  const smartLog = {}
  smartLog.criticalWarning = buffer.readUInt8(0)
  smartLog.compositeTemperature = buffer.readUInt16LE(1)
  smartLog.availableSpare = buffer.readUInt8(3)
  smartLog.availableSpareThreshold = buffer.readUInt8(4)
  smartLog.percentageUsed = buffer.readUInt8(5)
  smartLog.enduranceGroupCritical = buffer.readUInt8(6)

  smartLog.dataUnitRead = utils.bit.mergeBigInteger([
    buffer.readUInt32LE(32),
    buffer.readUInt32LE(36),
    buffer.readUInt32LE(40),
    buffer.readUInt32LE(44)
  ])

  smartLog.dataUnitWritten = utils.bit.mergeBigInteger([
    buffer.readUInt32LE(48),
    buffer.readUInt32LE(52),
    buffer.readUInt32LE(56),
    buffer.readUInt32LE(60)
  ])

  smartLog.numHostReadCmd = utils.bit.mergeBigInteger([
    buffer.readUInt32LE(64),
    buffer.readUInt32LE(68),
    buffer.readUInt32LE(72),
    buffer.readUInt32LE(76)
  ])

  smartLog.numHostWriteCmd = utils.bit.mergeBigInteger([
    buffer.readUInt32LE(80),
    buffer.readUInt32LE(84),
    buffer.readUInt32LE(88),
    buffer.readUInt32LE(92)
  ])

  smartLog.controllerBusyTime = utils.bit.mergeBigInteger([
    buffer.readUInt32LE(96),
    buffer.readUInt32LE(100),
    buffer.readUInt32LE(104),
    buffer.readUInt32LE(108)
  ])

  smartLog.numPowerCycles = utils.bit.mergeBigInteger([
    buffer.readUInt32LE(112),
    buffer.readUInt32LE(116),
    buffer.readUInt32LE(120),
    buffer.readUInt32LE(124)
  ])

  smartLog.hoursPowerOn = utils.bit.mergeBigInteger([
    buffer.readUInt32LE(128),
    buffer.readUInt32LE(132),
    buffer.readUInt32LE(136),
    buffer.readUInt32LE(140)
  ])

  smartLog.numUnsafeShutdown = utils.bit.mergeBigInteger([
    buffer.readUInt32LE(144),
    buffer.readUInt32LE(148),
    buffer.readUInt32LE(152),
    buffer.readUInt32LE(156)
  ])

  smartLog.numUnrecoveredIntegrity = utils.bit.mergeBigInteger([
    buffer.readUInt32LE(160),
    buffer.readUInt32LE(164),
    buffer.readUInt32LE(168),
    buffer.readUInt32LE(172)
  ])

  smartLog.numErrLifecycle = utils.bit.mergeBigInteger([
    buffer.readUInt32LE(176),
    buffer.readUInt32LE(180),
    buffer.readUInt32LE(184),
    buffer.readUInt32LE(188)
  ])

  smartLog.warningCompositeTempTime = buffer.readUInt32LE(192)
  smartLog.criticalCompositeTempTime = buffer.readUInt32LE(196)
  return smartLog
}

nvmeExt.parseFirmwareSlotInformation = function (buffer) {
  const activeFirmwareInfo = buffer.readUInt8(0)
  const nextActiveSlot = (activeFirmwareInfo & 0x70) >> 4 // Bits 6:4
  const currentActiveSlot = activeFirmwareInfo & 0x07 // Bits 2:0

  // Retrieve firmware revision for each slot
  const firmwareRevisions = []
  for (let i = 0; i < 7; i++) {
    // As there are 7 slots
    const offset = 8 + i * 8
    firmwareRevisions.push(buffer.slice(offset, offset + 8).toString('ascii'))
  }

  return {
    nextActiveFirmwareSlot: nextActiveSlot,
    currentlyActiveFirmwareSlot: currentActiveSlot,
    firmwareRevisions: firmwareRevisions
  }
}
