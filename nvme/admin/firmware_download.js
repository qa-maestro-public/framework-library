/**
 * Sends an NVMe Firmware Image Download command.
 * @param {Buffer} dataBuffer - Data buffer containing the firmware image (or its part) to be transferred.
 * @param {Number} offset - Number of dwords offset from the start of the firmware image being downloaded.
 * @returns {nvme.CompletionQueueEntry} - Returns the result of the NVMe command.
 */
nvme.firmwareImageDownload = function (dataBuffer, offset) {
  // Define the opcode for Firmware Image Download
  const OPC_FIRMWARE_IMAGE_DOWNLOAD = 0x11

  // Ensure dataBuffer size is a multiple of 4 (since 1 dword = 4 bytes)
  if (dataBuffer.length % 4 !== 0) {
    throw new Error('Size of dataBuffer is not a multiple of 4.')
  }

  // Derive numDwords from dataBuffer size and convert it to 0-based value
  const numDwords = dataBuffer.length / 4 - 1

  // Construct the command based on the NVMe spec details provided
  const command = {
    nsid: 0, // Setting to 0 as not used in the spec for this command
    opc: OPC_FIRMWARE_IMAGE_DOWNLOAD,
    cdw10: numDwords,
    cdw11: offset
  }

  // Based on the spec, Firmware Image Download command uses a data buffer, so isHostToDevice should be set to true
  return nvme.sendAdminCommand(command, dataBuffer, true)
}

/**
 * Sends an NVMe Firmware Commit command.
 * @param {Number} ca - Commit Action as defined in the NVMe spec.
 * @param {Number} fs - Firmware Slot as defined in the NVMe spec.
 * @param {Number} [bpid=0] - Boot Partition ID as defined in the NVMe spec. Optional.
 * @returns {nvme.CompletionQueueEntry} - Returns the result of the NVMe command.
 */
nvme.firmwareCommit = function (ca, fs, bpid = 0) {
  const OPC_FIRMWARE_COMMIT = 0x10

  // Construct the command based on the NVMe spec details you provided
  const command = {
    nsid: 0,
    opc: OPC_FIRMWARE_COMMIT,
    cdw10: (bpid << 31) | (ca << 3) | fs
  }
  const buffer = Buffer.alloc(0)

  // The Firmware Commit command, based on the spec, is likely a host-to-device command, hence setting isHostToDevice to true
  return nvme.sendAdminCommand(command, buffer, true)
}

/**
 * Perform firmware update.
 *
 * @param {String} firmwareFilePath - Path to the firmware file.
 * @param {Number} ca - Commit Action as defined in the NVMe spec.
 * @param {Number} fs - Firmware Slot as defined in the NVMe spec.
 * @param {Object} [options] - Additional options.
 * @param {Number} [options.bpid=0] - Boot Partition ID as defined in the NVMe spec. Optional.
 * @param {String} [options.baseDir='workspace'] - Base directory. It selects a base directory: 'log' is relative to the log directory, 'workspace' is relative to the current script.
 * @param {Number} [options.chunkSize=4096] - Size of each firmware chunk in bytes for the image download. It must be a multiple of 4.
 *
 * @returns {nvme.CompletionQueueEntry} - Returns the result of the Firmware Commit command.
 */
nvme.firmwareUpdate = function (firmwareFilePath, ca, fs, options = {}) {
  const { bpid = 0, baseDir = 'workspace', chunkSize = 4096 } = options

  // 1. Read the firmware file
  logger.info(`Reading firmware data from ${firmwareFilePath}`)
  const firmwareData = utility.readFile(firmwareFilePath, { baseDir })

  logger.info(`Firmware data size: ${firmwareData.length}`)
  if (chunkSize % 4 !== 0) {
    throw new Error('Chunk size must be a multiple of 4.')
  }

  // 2. Split the read firmware file into chunks and send each chunk using firmwareImageDownload
  logger.info('Sending firmware image to the target device')
  for (let i = 0; i < firmwareData.length; i += chunkSize) {
    const chunk = firmwareData.slice(i, i + chunkSize)
    const offset = i / 4 // Convert bytes offset to dwords offset

    nvme.firmwareImageDownload(chunk, offset)
  }

  // 3. Issue the firmwareCommit command after sending all chunks
  logger.info('Commiting firmware')
  return nvme.firmwareCommit(ca, fs, bpid)
}
